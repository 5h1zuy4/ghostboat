package shizuya.ghostboat;

import java.io.File;
import java.io.FileReader;
import java.awt.Color;
import java.io.BufferedReader;
import java.util.List;
import java.util.ArrayList;

import org.lwjgl.glfw.GLFW;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.vehicle.BoatEntity;
import net.minecraft.util.math.Vec3d;
import me.x150.renderer.event.RenderEvents;
import me.x150.renderer.render.Renderer3d;

public class GhostBoat implements ClientModInitializer {

    private final KeyBinding TOGGLE = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.ghostboat.toggle", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_LEFT_BRACKET, "ghostboat"));
    private final KeyBinding RESET = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.ghostboat.reset", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_RIGHT_BRACKET, "ghostboat"));

    public static MinecraftClient client = null;
    private static String fileLoaded = "";
    private static List<Vec3d> posList = new ArrayList<Vec3d>();
    private static List<Vec3d[]> coordList = new ArrayList<Vec3d[]>();
    private static List<Color> colorList = new ArrayList<Color>();
    private static int tick = 0;
    private static boolean wasInBoat = false;
    
    @Override
    public void onInitializeClient() {
        client = MinecraftClient.getInstance();
		AutoConfig.register(GhostBoatConfig.class, GsonConfigSerializer::new);
        ClientTickEvents.END_CLIENT_TICK.register(this::onEndClientTick);
        RenderEvents.WORLD.register(GhostBoat::world);
    }

    private void onEndClientTick(MinecraftClient client) {
        if (client.player != null) {
            if (client.player.getVehicle() instanceof BoatEntity) {
                if (!wasInBoat) {
                    wasInBoat = true;
                    tick = 0;
                }
            } else {
                wasInBoat = false;
            }
        }
        if (RESET.wasPressed()) {
            tick = 0;
        }
        if (TOGGLE.wasPressed()) {
            GhostBoatConfig.getConfig().toggle();
        }
        if (GhostBoatConfig.getConfig().isEnabled()) {
            if (GhostBoatConfig.getConfig().getFilename() != fileLoaded) {
                loadTelemetry();
                fileLoaded = GhostBoatConfig.getConfig().getFilename();
            }
        }
        ++tick;
    }

    private void loadTelemetry() {
        posList.clear();
        colorList.clear();
        try {
            BufferedReader br = new BufferedReader(new FileReader(GhostBoatConfig.getConfig().getDirectory() + File.separator + GhostBoatConfig.getConfig().getFilename()));
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                if (line.contains("speed")) continue;
                String[] row = line.split(",");
                double x = Double.parseDouble(row[2]);
                double y = Double.parseDouble(row[4]) + 0.01;
                double z = Double.parseDouble(row[3]);
                posList.add(new Vec3d(x, y, z));
                Vec3d[] coordArray = new Vec3d[8];
                for (int i = 0; i < 8; ++i) {
                    coordArray[i] = new Vec3d(x + ((i & 1) - 0.5) * 1.375, y + (i >> 2 & 1) * 0.5625, z + ((i >> 1 & 1) - 0.5) * 1.375);
                }
                coordList.add(coordArray);
                double steering = Double.parseDouble(row[6]);
                double throttle = Double.parseDouble(row[7]);
                float h = 0f;
                float s = 1f;
                float b = 0f;
                if (steering == 0) {
                    s = 0f;
                } else if (steering > 0) {
                    h = GhostBoatConfig.getConfig().getLeftHue();
                } else {
                    h = GhostBoatConfig.getConfig().getRightHue();
                }
                if (throttle == 0) {
                    b = 0.5f;
                } else if (throttle > 0) {
                    b = 1f;
                } else {
                    b = 0.25f;
                }
                colorList.add(Color.getHSBColor(h, s, b));
            }
            br.close();
        }
        catch (Exception e) {
        }
    }

    public static void world(MatrixStack stack) {
        if (GhostBoatConfig.getConfig().isEnabled()) {
            if (tick >= posList.size()) return;
            Vec3d curr = posList.get(tick);
            if (curr.distanceTo(new Vec3d(client.player.getX(), client.player.getY(), client.player.getZ())) > GhostBoatConfig.getConfig().getRange()) return;
            Vec3d[] coordArray = coordList.get(tick);
            Color c = colorList.get(tick);
            for (int i = 0; i < 4; ++i) {
                Renderer3d.renderLine(stack, c, coordArray[i], coordArray[i + 4]);
            }
            for (int i = 0; i < 8; i += 4) {
                Renderer3d.renderLine(stack, c, coordArray[0 + i], coordArray[1 + i]);
                Renderer3d.renderLine(stack, c, coordArray[1 + i], coordArray[3 + i]);
                Renderer3d.renderLine(stack, c, coordArray[3 + i], coordArray[2 + i]);
                Renderer3d.renderLine(stack, c, coordArray[2 + i], coordArray[0 + i]);
            }
        }
    }
}
