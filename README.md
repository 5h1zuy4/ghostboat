# Ghostboat

### Displays a ghost boat following the boat's path from a telemetry file.

## Options:

**Directory** + **Filename**: Path of the telemetry file.      
**Range**: Ghost will be rendered if it is within this distance (blocks).      
**Left/Right Hue**: Shift the hue of the ghost to this angle (degrees) when left/right key was pressed. 